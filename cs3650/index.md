# CS3650: Computer Architecture

The following are my class notes for Computer Architecture.

# Topics

1. [Logic Gates](01 - Logic Gates.html)
2. [Half Adder and Full Adder](02 - HA FA.html)
3. [Approximate Computation](03 - Approximate Computation.html)
4. [K-map](06 - Kmap.html)
