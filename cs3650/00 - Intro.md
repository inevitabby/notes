---
title: "Introduction"
---

# Outline?

## Rambling Man

Adder:
- Extremely easy to make it.
- Can do subtraction (add negative)
- Can do multiplication (add multiple times)
- Can do division (subtracting multiple times)

Flip-Flop:
- Simplest unit of memory cell

Pipelining:
- Situation: Reading instruction from memory to CPU, decoding it, executing it, and writing back to register
	1. Fetch
	2. Decode
	3. Execute
	4. Write-Back
- Suppose it takes a minute to do every action, then two instructions will take 8 minutes.
- Solutions to this are pipelining and superscaling.
	- What are they? I don't know.

Caches:
- New computers (>10 years) have 3 layers of cache.
	- I don't know what this means.

???:
- SISD: Single Instruction Single Data
- SIMD: Single Instruction Multiple Data
- MIMD: Multiple Instruction Multiple Data

Synchronous: Whole is system is controlled with a ??? clock.

Polling v.s. Interrupt:
1. Interrupt: Mid-lecture, a student asks a question, and the professor interrupts the lecture to answer.
	- Better the polling 99% of the time
3. Polling: The professor asks every student "do you have a question?".

Faulty Devices v.s. Interrupt/Polling:
- A broken device that doesn't send an interrupt will never be found.
- A broken device that doesn't respond to polling will be found

Tuesday: Teach
Thursday: Teach and Discuss
- Divide into groups of four.

<!--
WISDOM:
What if instead of Computer Architecture,
it was called Freaky Architecture
-->
