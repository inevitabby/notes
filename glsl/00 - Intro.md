---
title: "Intro"
---

# OpenGL

**OpenGL**: Specification for manipulating graphics and images maintained by the Khronos Group.
- OpenGL by itself is a large state machine.
	* OpenGL Context: The state of OpenGL
	* There are *state-changing* and *state-using* functions.




