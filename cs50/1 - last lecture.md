# Last Lecture

We covered:
- Functions
	* args, return values
- Conditionals
- Booleans
- Loops
- Variables
- ...

This lecture we'll be translating these ideas into the C language.
