# Header File

A menu of all the available functions that you can include in your c file
- e.g., `stdio.h`

A header file is the very specific mechanism by which you include a library. Libraries and header files are not the same thing.
- e.g., The standard input library is a library; the header file you use to import it is `stdio.h`

