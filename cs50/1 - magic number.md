# Magic Number (common mistake)

When you hardcode a number instead of using a variable
- e.g., using a constant like `const int MINE = 2`. Capitalizing constants is common practice—and a good way to solve magic numbers.
