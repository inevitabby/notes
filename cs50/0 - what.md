# What is Computer Science?

- Computer science is about creation and problem-solving
	* It's about thinking more precisely, correctly, and methodically
	* Problem-solving is about input $\to$ output
- *Computer science is not working on computers in isolation*
- **What ultimately matters in this course is where you end up relative to yourself where you began**
