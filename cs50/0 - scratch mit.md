# Scratch.mit

We'll be using scratch—a block-based programming language—to explore programming fundamentals without the syntax.

Takeaways:
- Better design means less repetition in the code, easier maintainability
