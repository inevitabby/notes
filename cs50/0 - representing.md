# Possible Ways of Representing __ in Binary:

## Colors

We can describe colors in RGB (e.g., one byte for every color, 24 bits in total) and assign one color to every pixel

## Video

A sequence of images (colors in a grid)

## Audio

A sequence of notes (e.g., midi) with pitch, length, and volume.

# Summary: Representing Information

We can represent many types of information as long as we agree on the standards.

