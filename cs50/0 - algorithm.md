# Algorithm

A step-by-step instruction for solving a problem
- **Software/Programs**: one or more algorithms implemented in a computer
- e.g., a binary search algorithm to find a person in a contact list
