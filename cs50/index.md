# Harvard CS50: Introduction to Computer Science

# Lecture 0: Scratch

**Starter**:
- [What is Computer Science?](0 - what.html)

**Binary & Information**:
- [Binary, Bits, & Bytes](0 - binary.html)
- [Representing Letters in Binary (ASCII)](0 - ascii.html)
- [Possible Ways of Representing __ in Binary:](0 - representing.html)

**Other Definitions / Introductions**:
- [Algorithm](0 - algorithm.html)
- [Pseudocode](0 - Pseudocode.html)
- [Scratch.mit](0 - scratch mit.html)

# Lecture 1: C

**Starter**:
- [Last Lecture](1 - last lecture.html)
- [Learning New Languages](1 - learning.html)

**On Tools**:
- [Integrated Desktop Environments](1 - ide.html)
- [Compiler](1 - compiler.html)
- [This Class's Tools](1 - this class.html)
- [Make](1 - make.html)
- [Unix](1 - unix.html)

**C**:
- [C](1 - c lang.html)
- [Functions](1 - functions.html)
	* [int main void](1 - main.html)
- [Header File](1 - header file.html)
	* [CS50 Library](1 - cs50.html)
- [Types](1 - types.html)
	* [Bit Limitations](1 - bit limitations.html)
	* [Type Conversion](1 - type conversion.html)
- [Operators](1 - operators.html)
- [Syntactic Sugar](1 - syntactic sugar.html)
- [Comment](1 - comment.html)
- [Conditionals](1 - conditionals.html)
	* [and or](1 - and or.html)
- [Common Bug: Magic Number](1 - magic number.html)
- [Quotes](1 - quotes.html)
- [Loops](1 - loops.html)
	* [do while loop](1 - do while loop.html)
- [Scope](1 - scope.html)
- [Overflow](1 - overflow.html)
	* [Floating Point Imprecision](1 - floating point imprecision.html)

# Lecture 2: Arrays

**Compiling & Debugging**:
- [Make](2 - make.html)
- [Compiling Process](2 - compiling process.html)
- [Debugging Process](2 - debugging process.html)

Arrays:
- [Array](2 - array.html)

**Args and Exit Codes**:
- [Command-line Arguments](2 - cli arg.html)
- [Exit Status](2 - exit status.html)

**Misc**:
- [Readability](2 - readability.html)
- [Cryptography](2 - cryptography.html)

# Lecture 3: Algorithms

**Starter**:
- [Recall](3 - recall.html)
- [Visualizing Arrays](3 - visualizing arrays.html)
- [Searching (Problem Statement)](3 - searching problem statement.html)

**Measuring Efficiency**:
- [Determining Efficiency](3 - determining efficiency.html)

**Search Algorithms**:
- [Search Algorithms](3 - search algorithms.html)
- [Code Examples: Linear Search](3 - code examples linear search.html)

**Data Structures**:
- [Data Structures](3 - data structures.html)

**Sorting Algorithms I**:
- [Sorting Algorithms](3 - sorting algorithms.html)

**Recursion**:
- [Recursion](3 - recursion.html)

**Sorting Algorithms II**:
- [Merge Sort](3 - merge sort.html)

# Lecture 4: Memory

**Starter**:
- [Starter](4 - starter.html)
- [Image](4 - image.html)

**Memory & Pointers**:
- [Counting Memory](4 - counting memory.html)
	* [Hexadecimal](4 - hexadecimal.html)
- [Pointers](4 - pointers.html)
	* [Strings Revisited](4 - strings revisited.html)
	* [Pointer Arithmetic](4 - pointer arithmetic.html)
- [Dynamic Memory Allocation](4 - dynamic memory allocation.html)

**Memory Debugging**:
- [Valgrind](4 - valgrind.html)
- [Garbage](4 - garbage.html)

**Heap & Stack**:
- [Physical Memory](4 - physical memory.html)
- [Example: Function Variable Scope](4 - example 1.html)
- 4 - heap and stack

**Input & Output**:
- [Scanf](4 - scanf.html)
- [File Pointers](4 - file pointers.html)

# Lecture 5: Data Structures

# Lecture 6: Python

# Lecture 7: Sql

# Lecture 8: HTML, CSS, Javascript

# Lecture 9: Flask

# Lecture 10: Emoji

# Emoji
