# Searching (Problem Statement)

How might we determine whether something is in an array?

Input $\to$ Output
- Array $\to$ Boolean (is "X" in the array?)

