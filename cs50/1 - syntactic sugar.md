# Variables, Syntactic Sugar

Example of using syntactic sugar:
```c
int counter = 0;
counter = counter + 1; # Notice how this demonstrates that = isn't equality, it's assignment (right to left)
```

```c
int counter = 0;
counter++;
```

