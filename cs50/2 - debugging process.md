# Debugging Techniques

- Inject `printf` statements to monitor the value of variables
- Use a debugger
- Rubber duck debugging (explain your code out-loud)
