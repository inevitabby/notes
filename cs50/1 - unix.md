# Unix Commands

## Change Directory

- `cd`: Change working directory
- `..`: Parent directory
- `.`: Current directory
