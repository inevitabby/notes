# Types

- bool: True false
- char: Single char
- double: More precise than a float
- float: Number with a decimal point
- int: Integer
- long: integer with more bits
- string: ???
- ...

- When changing a variable that has already been defined, the type doesn't need to be redeclared.
- So long as one of the values in an arithmetic calculation are a long, the others will be promoted to longs as well.

