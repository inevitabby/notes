# C

- `=` is the assignment operator
	* read from right to left
- All variables need a data type
	* e.g., string, int, etc.
	* How we tell the computer how to interpret the binary
	* The compiler will error out if you mismatch types
- Case insensitive
	* In many languages it is common practice to have lowercase names. Lots of different styles exist, though.
- Libraries must be loaded to do things
- `%s` placeholder code for a string. This is a particularly common paradigm in languages.

