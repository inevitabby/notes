# Pseudocode

A way to describe an algorithm formally without a strictly defined programming language.

> **Note**: Remember to handle edge cases!

Some commonalities among programming languages described in pseudocode:
- Functions: Actions or verbs that solve a smaller problem
- Conditionals: Things we do conditionally, based on the answer to a question
- Boolean Expressions: Questions that give true or false (1 or 0) answers
- Loops: A cycle that tells us to do something again and again
