# Compiler

A tool for converting source code to machine code.

- Not all languages use compilers
- Meant to be correct and precise in its errors, not necessarily user-friendly
