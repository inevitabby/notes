
# Bit Limitations

Data types have bit limitations.

32-bits have roughly 4 billion permutations. If you want to support positive is negative numbers, then that means the largest number your can fit in 32-bits in 2 billion and -2billion

Using a long over an int allows us to solve bigger problems, but it only kicks the can down the road.
