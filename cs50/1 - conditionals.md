# Conditionals

```
if (conditional)
{
	do stuff
}
```

Note that `if` is a construct of the language, not a function, despite following the same `word()` format as a function.

Best practice: always use curly braces to wrap statements.
