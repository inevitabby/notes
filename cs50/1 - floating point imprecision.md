# Floating-point Imprecision

Computers are—fundamentally—unable to represent all real numbers with 100% precision.

In scientific computation we can use more bits to get more precise calculations
