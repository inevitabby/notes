---
title: "I/O Programming"
---

**PMIO**: Port-Mapped I/O
- Special Instructions

**MMIO**: Memory-Mapped I/O
- Memory load/store instructions
	- *(you can just use `lw` and `sw` to interact)*

There are four technique for I/O programming:

1. Programming I/O (PIO)
	- With busy-waiting
	- With polling
2. Interrupt
3. Direct Memory Access or a special processor for I/O called I/O processor.
	- CPU can program the hardware.


