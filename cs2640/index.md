# CS2640: Computer Organization and Assembly Programming

The following are my class notes for Computer Organization and Assembly Programming.

# Topics

## I. Introduction

1. [Programming Guidelines](00 - Programming Guidelines.html)
2. [Intro](01 - Intro.html)

## II. Number Systems and Binary

3. [Number Systems and Conversions](02 - Number Systems and Conversions.html)
4. [Binary Operations and Sign](03 - Binary Operations and Sign.html)

## III. Computer Systems and Architecture

5. [Computer Systems](04 - Computer Systems.html)
6. [Von Neumann Architecture](05 - Von Neumann Architecture.html)
7. [MIPS Architecture](06 - MIPS.html)

## IV. ISA and Assembler

8. [Instruction Set Architecture](07 - ISA.html)
9. [Assembler, Linker, and Loader](08 - Assembler Linker Loader.html)

## V. MIPS Assembly

10. [MIPS Assembly](09 - SPIM.html)
11. [Midterm I Review](10 - Midterm I Review.html)
12. [Array and Dynamic Memory](11 - Array and Dynamic Memory.html)
13. [Procedures](12 - Procedures.html)
14. [Floating-Points](13 - FPU.html)
