---
title: "Intro"
---

# Overview

- **Number Representation**
	* e.g., $123 = 1 \times 10^2 + 2 \times 10^1 + 3 \times 10^0$
- **Integer Arithmetic**
	* We'll spend most of our time (~13 weeks) with integer arithmetic.
- **Von Neumann Machine**
	* aka: Harvard architecture machine
	* We won't be learning about the Princeton architecture
- **ISA (Instruction Set Architecture)**
	* The assembly language for a particular CPU
		+ e.g., MIPS, x86, ARM, etc.
	* We'll be using MIPS in this class
- **Addressing Modes**
	* How you access (read/write) memory
	* Other names: RAM, RWM (Read Write Memory), Main Memory
	* e.g., x86 has 5—6 ways to access memory while MIPS has one.

- **Assembly**
- **Arrays and Records**
	* Arrays are homogeneous
	* Records are heterogeneous
- **Subroutines and Macros**
- **Interrupts**
- **I/O Interfacing**
- **Communication**

# Grades

- **25%: Homework/Quizzes**
	* Weekly quizzes on Friday (10—15 minutes)
	* Homework assigned Wednesday, due Friday
	* Usually due before start of class
- **30%: Projects (4x)**
	* Usually due before start of class
- **20%: Midterm Exams (2x)**
	- Midterm 1: Week 6, March 1
	- Midterm 2: Week 10, before Spring Break
- **25%: Final Exam**

# Resources

- SPIM, A MIPS32 simulator
- Computer Organization & Design MIPS Edition, Patterson & Hennessy (2021)
