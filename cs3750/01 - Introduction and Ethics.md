---
title: "Introduction & Ethics"
---

# The Pace of Change

> **Example**:
> - 1940s: First computer built
> - 1956: First hard-drive weighted a ton and stored 5 Mb.
> - 1991: Space shuttle had a one-megahertz computer.
> - Present: Several gigahertz clock speeds.

# Change and Unexpected Developments

**Betty Friedan**: The distinction between human and animal behavior is to build and design the world.

**Cell Phones**
- Relatively few in the 1990s $\to$ 5 billion in 2011
- Used For:
	* Conversations & messaging
	* Pictures, Music, Videos
	* Email
	* Games
	* Banking, Investment
	* Maps
	* Etc.
- Unexpected Developments:
	* Privacy concerns 
		+ Location
		+ Cameras
		+ Etc.
	* Solitude, quiet, concentration, distracted driving
	* Underage sexting, enabling crime.

**Social Networking**:
- First online social networking site: Classmates.com (1995)
- MySpace (2003) had 100 million member profiles by 2006
- Facebook was started at Harvard as an online version of student directories
- Social networking lets hundreds of millions of people easily share parts of their lives.
- Used For:
	* Businesses connect with customers
	* Organizations seek donations
	* Groups organize volunteers
	* Protests organize demonstrations
	* Crowd funding.
- Unexpected Developments:
	* Stalkers and bullies
	* Jurors leak court case information online
	* Bots simulate humans.

**Communication and the Web:**
- Emails were short and contained only text in the 1980s
- In the present day we can text, tweet, email, and use social media.
- Blogs started as outlets for amateurs to express ideas but became significant sources of news and entertainment.
	* Etymology: Web Log
- Inexpensive video cameras and editing tools allowed for amateur videos.
- Many videos on the web can infringe copyrights owned by entertainment companies.

**Telemedicine:**
- Remote performance of medical exams and procedures, including surgery.

**Collaboration:**
- Wikipedia: The online, collaborative encyclopedia written by volunteers.
- Informal communities of programmers create and maintain free software.
- Watch-dog on the web: Informal decentralized groups of people help investigate crimes.

**E-Commerce:**
- Amazon.com (1994) started selling books and grew to be one of the most popular commercial sites.
- eBay.com facilitates online auctions.
- Traditional brick-and-mortar-businesses have established websites.
- Online sales in the US now total hundreds of billions of dollars a year.
- Sellers can sell directly to buyers, resulting in a peer-to-peer economy.
- Trust Concerns:
	* People reluctant to provide credit card information for online purchases use PayPal as a trusted intermediary.
	* Encryption and secure servers makes payments safer.
	* The Better Business Bureau have a website to let consumers see if others have complained about a business.
	* Auction sites implemented rating systems.

**Free Stuff:**
- Email programs & accounts, browsers, filters, firewalls, encryption software, word processors, spreadsheets, software for viewing documents, software to manipulate photos and video, etc.
- Phone services using VOIP like Skype.
- Craiglist classified ad site.
- University lectures.
- Funding:
	* Advertising pays for many "free" sites and services, but not all.
	* Wikipedia funded through donations
	* Businesses provide some services for good public relations and as a marketing tool.
	* Generosity and public service flourish on the web. Many people share their expertise just because they want to.
- Advertising:
	* Many sites collection information about our online activities and sell it to advertisers.

**Artificial Intelligence:**
- Branch of computer science that lets computers perform tasks normally requiring human intelligence.
- Researchers realized that narrow, specialize skills were easier for computers.
	- Many AI applications involve pattern recognition.
	- Speech recognition is now a common tool.
- Turing Test: If the computer convinces the human subject that the computer is human, the computer is said to "pass".

> **Discussion Questions:**
> 
> - How will we react when we can go into a hospital for surgery performed entirely by a machine? Will it be scarier than riding in the first automatic elevators or airplanes?
> - How will we react when we can have a conversation and not know if we are conversing with a human or a machine?
> - How will we react when chips implanted in our brains enhance our memory with data and a search engine? Will we still be human?

**Robots:**
- Mechanical devices that perform physical tasks traditionally done by humans.
- Can operate in environments that are hazardous for people.

**Smart Sensors, Motion, and Control:**
- Motion sensing devices are used to give robots the ability to walk, trigger airbags in a crash, etc.
- Sensors can detect leaks, acceleration, position, temperature, and moisture.

**Tools for Disabled People:**
- Assistive technology devices help restore productivity and independence.

# Themes of Technology: Challenges

**Challenges:**
- Old problems in a new context: Crime, pornography, violent fiction.
- Adapting to new technology: Thinking in a new way.
- Varied sources of solution to problems: The natural part of change life.
- Global reach of net: Ease of communication with distant countries.
- Trade-offs and controversy: Increasing security means reducing convenience.
- Perfection is a direction, not an option.
- There is a difference between personal choices, business policies, and law.

**Ethics:**
- Study of what it means to "do the right thing"
- Assumes people are rational and make free choices.
- Rules to follow in our interactions and our actions that affect others.
- A Variety of Ethical Views:
	1. **Negative Rights (Liberties)**:
		- The right to act without interference.
	2. **Positive Rights (Claim-Rights)**:
		- An obligation of some people to provide certain things for others.
	3. **Golden Rules**:
		- Treat others as you would want them to treat you.
	4. **Contributing to Society**:
		- Doing one's work honestly, responsibly, ethically, creatively, and well is virtuous.
	5. **No Simple Answers**:
		- Human behavior and real human situations are complex. There are often trade-offs to consider.
		- Ethical theories help to identify important principles or guidelines.
		- Do Organizations Have Ethics?
- **Important Distinctions**:
	* Right, wrong, and okay
	* Distinguishing wrong and harm
	* Separating goals from constraints
	* Personal preference and ethics
	* Law and ethics
