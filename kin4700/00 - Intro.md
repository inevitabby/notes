**Emotional Wellbeing**: Our day-to-day function.
- You get better at feeling what you feel the most.
- Every day you carry an imprint of what you felt.

**Stress Prevention**: 

**Two Dominant Drives**:
- Fear Response
- Calm and Connection

**Diversity and Emotional/Mental Health**:
- Mental health shouldn't be a White middle-class thing.

MindHeart

What Emotion

What You Learned

What Activity You Did

Short Practice

Long Practice

"You are your synapses"
— Joseph LeDeux

Nerve cells that no longer fire together no longer wire together.

**Cortisol Physical Side-Effects**:
- Increased:
	- Mortality
	- Heart Disease/Stroke
	- Body fat
	- Blood sugar
- Chronic disease
- Reduced immune system function
- Pre-mature aging
- High blood pressure.
- Disrupted sleep
- Acne
- Headaches
- Back Pain

**Cortical Inhibition**: Difficulty storing information or using executive functions. 
- Also shrinks the hippocampus.

**Long-Term Adaptation**:
1. Proliferation of Corticotropic Cells: If body produces too much ACTH, more cells to produce ACTH are created.
2. Upregulation: More cortisol receptor sites are made.
3. 5HTT-SERT: Hyperreactive to cortisol and serotonin.

**Cortisol's Half Life**: 12.5 hours

**The Physiology of a Breath**:
- Reduces cortisol
- Activates parasympathetic nervous system
- Increase oxytocin
- Changes quality and type of peptides released from brain (serotonin, dopamine, etc.)
- Improves heart coherence.
