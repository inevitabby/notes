---
title: "Defining the System Architecture"
---

# Anatomy of Modern Systems

## Computing Devices

Server: Manages shared resources and enables users and other computers access to these resources.
- Server Farm: For very large databases and high use.

Personal computing devices or clients:
- e.g. desktops, laptops, tablets, smartphones.

## Networks

**Computer Network**: Hardware, software, transmission media

**Internet Backbone**: High-capacity with high-bandwidth trunk lines and large high-speed computers.

**Local area network (LAN)**: Small network for a single site.

**World Wide Web (WWW)**: All the interconnected resources accessed through the internet.

**Uniform Resource Locator (URL)**: Identifier for the Web to locate a particular resource.

**Hyperlink**: URL of a resource embedded within another resource.


