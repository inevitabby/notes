---
title: "Introduction"
---

**Group Project**: There's a semester-long group project.
- This project will be CV-worthy.
- We'll be using GitHub to collaborate.
- There'll be one group representative.
	* Late submissions penalize everyone.

**Books**:
- **Reference Book**: *Object-Oriented Modeling and Design with UML*, 2nd ed
- **Lecture Book**: *Systems Analysis and Design in a Changing World*, 7th ed

**Grades**: Grades are curved.
- Group work is 45% of total grade.

