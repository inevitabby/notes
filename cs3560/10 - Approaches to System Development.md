---
title: "Approaches to System Development"
---

# Two General Approaches

1. Predictive Approach: Project is planned in advance and the information system is developed according to the plan.
	- Waterfall
	- Requirements are well-understood and/or low technical risk.
2. Adaptive Approach: Project is more flexible and adapt to changing needs as the project progresses.
	- Iterative
	- Requirements are uncertain and/or high technical risk.

# AGILE Modeling Principles

12 Principles of Agile Modeling:
- Philosophy: Build only necessary models that are useful and at the right level of detail.

# Extreme Programming

Take the best practices of software development and extend them to the extreme.
- Focus intensely on proven industry practices.
- Combine them in unique ways to get better results.

Core Values:
- Communication
- Simplicity
- Feedback
- Courage
