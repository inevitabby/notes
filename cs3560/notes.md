---
title: "Object-Oriented Programming in One Page"
---

# Programming Paradigms

**Programming Paradigms**: A strategy we follow to program.

## A. Imperative

**Imperative**: List of instructions tell computer what to do step-by-step.
- Describe explicitly which steps to perform to get the solution.
- *e.g., Java, C, Python, Ruby, etc.*
- Analogy: Telling your friend step-by-step how to fix your car.

## B. Declarative

**Declarative**: Express the logic of computation without talking about its control flow.
- Desired result is described directly.
- *e.g., HTML, SQL, Prolog, Lisp, etc.*
- Analogy: Telling your friend to fix your car but don't care about the steps.
- Two major programming paradigms that are declarative: 
	* Functional programming 
	* Logic programming.
