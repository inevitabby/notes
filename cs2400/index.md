# CS2400: Data Structures and Advanced Programming

## I. Introduction

0. [Coding Guidelines](00 - Requirements.html)
1. [Intro](01 - Intro.html)
2. [Review](02 - Arrays.html)

## II. Classes, Abstract Data Types, and Generics

3. [Designing Classes](03 - Designing Classes.html)
4. [Abstract Data Types](04 - Abstract Data Types.html)
5. [Generics](05 - Generics.html)

## III. Fixed-Size Array and Linked List

6. [Bag Implementation: Fixed-Size Array](06 - Bag Implementations That Use Arrays.html)
7. [Bag Implementation: Linked List](07 - Linked List Bag Implementation.html)

## IV. Iterators

8. [Iterators](08 - Iterators.html)

## V. Algorithm Efficiency

9. [Algorithm Efficiency](09 - Algorithm Efficiency.html)

## VI. Stack

10. [Stack](10 - Stack ADT.html)


## VII. Queue

11. [Queue](11 - Queue ADT.html)

## VIII. Recursion

12. [Recursion](12 - Recursion.html)

## IX. List

13. [List](13 - List ADT.html)


## X. Dictionary

14. [Dictionary](14 - Dictionary ADT.html)

## XI. Hashing

15. [Hashing](15 - Hashing.html)

## XII. Trees

16. [Trees](16 - Trees.html)
17. [Binary Search Tree](17 - BST.html)
18. [Heap](18 - Heap.html)
19. [Graph](19 - Graph.html)
