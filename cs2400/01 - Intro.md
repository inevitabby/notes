# Overview

> **Programming Language**: Java
> - There will be no Java introduction!

- **Linear and Non-linear Data Types**
- **Interfaces and Generics**
- **Advanced File Access**
	* Non-sequential binary files
	* Lots of database techniques
	* *(Might not be covered in this class.)*
- **Recursive Structures and Operations**
	* e.g., trees and graphs, linked list
- **Big O Notation**
- **Algorithm Analysis**
	* *(Introduction)*

> **Note**: Lots of focus on ADT (abstract data types)!
> - e.g., stack, queue, trees, graphs

# Resources

- Data Structures and Abstractions with Java, Carrano and Henry, 5th Edition (2019)

# Debugging

Debugging is a vital skill for this class.
