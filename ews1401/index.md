# EWS: Introduction to Ethnic Studies

The following are my notes for Introduction to Ethnic Studies

# Notes

1. [Introduction](00 - Intro.html)
2. [Notes on Chapter 3 of White Guys on Campus](WGOC.html)
3. [How-To Approach Readings](approach.html)
4. [Notes on Introduction: The Chicano Movement and Chicano Historiography](chicano-historiography.html)
