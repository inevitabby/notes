---
title: "Introduction"
---

**Course Description**: Foundational theories and concepts in Ethnic Studies that examine how social institutions have historically shaped race relations and social processes producing racial, class, social, and gender inequalities and stratifications. 

**Books**:
- *You Sound Like a White Girl*: Autobiography
- *White Guys on Campus*: Examination of racism in colleges.
	* We'll analyze in groups of 5.

**In-Class Papers**:
- Avoid bullet-point answers.
- Be concise and detailed.

**3 Major Assignments**:
1. *Dia de los Muertos* Community Ofrenda
	- One-page reflection + Art project
2. Art as Social Action-Social Justice Creative Artwork
3. *White Guys on Campus* Collaborative Learning Group Chapter Presentations

**How to Approach Readings**:
1. Main Points
2. Organization (e.g., thematically, chronologically)
3. Periodization (when is it taking place)
4. Regional Scope (where is it taking place)
5. Sources
6. Methodology
7. Weaknesses
8. Intended Audience
9. Does Author Accomplish Goals?

**Decolonization** is the paradigm shift from a culture of exclusion to making space for other political philosophies and knowledge systems.
- Things aren't normal.
- We'll focus on structural, institutional, systemic, and historic racism.

History stays with us.
- Little Rock Nine
- Brown Berets
- Weather Underground
- SDS
- I Wor Kuen
- Young Lords
- etc.

Era of Protest: 1949-1975
- Third World Liberation Front at San Francisco State.

**On the Intersectionality of White Supremacy and Eurocentric Education**:
- Students paddled and humiliated for not speaking English are an example of the intersectionality of Eurocentric education and White supremacy.
- Even without the paddling, dress code, removal of lockers, etc.

**La Fiesta de Muertos**:
- Pre-Columbian origins, a way to honor the ancestors.
	* Not "Mexican Halloween"
- Central nuclear of a people's identity and part of *a way of understanding the universe* which comes from various Central American and Mexican cultures.
	* For all these cultures, the ideas of sin/the devil/etc. is not native.
- There is attempted fusion of cultures due to colonialism and Catholicism.
- Not:
	- a celebration of death
	- "Mexican Halloween"
	- about face-painting
	- about dressing up in a scary/funny/sexy costume
	- about scaring children
	- passing out candy
	- grieving in a tragic, sorrowful manner
- About:
	- remembering loved ones and ancestors
	- community building
	- cultural and historical memory
	- resistance & social justice
	- Can be used to highlight political insurgency:
		* e.g., ofrendas for the mass-killing of women, covid deaths, mass violence, etc.

Nov 1: All Saints' Day
- For Children who've passed away.

Nov 2: All Souls' Day

**Existence is Resistance**

**Cempasuchil**: Marigold, flower of life.

Mictlantecuhtli: Mexica deity of the dead.
- Not feared
- Looked upon as a kind of deity who released peopel from the burdens and hasrshness of life.

El Papel Picado: Lace-like tiers of color paper
- The ofrenda is often trimmed with papel picado.
- Symbolizes the fragility and important of humane existence.

Pan de Muerto: Bread of the dead
- Version of a dough figure that topped a Mexica cermonial pole.

Whom to Remember:
- Family/Friend
- Historical Figure
- Social Justice
- Rights Issue

Ofrendas should have four levels/sides, representing the four seasons/directions/elements/stages of life.
