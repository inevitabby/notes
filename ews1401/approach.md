---
title: "How-To Approach Readings"
---

The following guiding questions can help with approaching the readings.

1. Main Argument(s)/Concept(s)
	- *Describe what they are.*
2. Organization?
	- *Chronological, thematic, etc.*
3. Periodization
	- *Time, year, decade, era, etc.*
4. Regional Scope
	- *Place, location, etc.*
5. Sources
	- *Oral history interviews, university archives, newspapers, photographs, participant-observation, etc.*
6. Methodology
	- *Qualitative, quantitative, mixed-methods research, etc.*
	- *Tools used to collect the information/data/story.*
7. Gaps, Areas of Improvement, Weaknesses
	- *How can the author improve the work?* 
	- *Who’s missing from the narrative?*
	- *Any limitations in the work?*
8. Intended Audience
	- *Who is the author writing for?*
		* *Why?*
		* *Is the author successful in reaching that audience?*
9. Does the author(s) accomplish goals? Explain.
