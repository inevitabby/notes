---
title: "Notes on Introduction: The Chicano Movement and Chicano Historiography"
---

**The Chicano Movement:**
- Largest and most widespread civil rights movement by Mexican-descent people in the US.

**Years**: 1965—1975 *(possibly ended later in the late 1970s or 1980s)*
- 1965: Grape strike of farmworkers in San Joaquin Valley led by Cesar Chaves and Dolores Huerta.
	* Brought wage and benefit increases, dignity, and national attention.
		+ National attention influenced Chicano youth throughout CA and the Southwest.
	* Laid groundwork for the **Chicano Generation**, a new political generation that spearheaded the Chicano Movement in Urban areas.
- 1975: The Vietnam War ends with the humiliating loss of the US. Protests of the 1960s spill over into the early 1970s.
	* End of the Vietnam War meant the beginning of the decline of anti-war protests, especially the militant dynamics of the movement.
	* Composition:
		+ Second-generation children of Mexican immigrants.
	* Groups:
			+ LULAC: League of United Latin American Citizens
			+ Mexican American Movement
			+ Spanish-Speaking Congress
			+ American GI Forum
			+ Community Service Organization
			+ Asociacion Nacional Mexico-Americana
			+ Mexican American Political Association
	* Goals:
		+ Desegregate so-called Mexican schools in the Southwest and public facilities.
		+ Better jobs and living wages for Mexican-origin workers
		+ Confronting jury discrimination, esp. in Texas.
		+ Post-War, pushed for greater voting and running in public offices.

Despite important reforms, deeply seated racialized class system made Mexican-origin people cheap and exploitable labor into the 1960s.
- The **New Chicano Generation** would fight the second-class system, along with focusing on anti-colonialism and [self-determination rather than integration]{.underline}.
	* **Chicano Power** means self-empowerment.


**Relationship between Chicano Movement and Chicano History**:
- Part of self-empowerment is having a sense of history.
- Mexican history suffers from erasure in US schools.
- Thus, exploring indigenous and mestizo past and discovering erased history was paramount.
	* The Chicano Movement did not exist in a vacuum.
	* Conquered Generation: Result of US-Mexico War (1846—1848) that led to the annexation of close to half of Mexico.
		+ Mexicans who found themselves above the new border were treated as second-class US citizens.
		+ Lands were stolen and Mexicans were turned into a cheap exploitable class by Capitalism.
		+ Revolt was repressed.
	* Immigrant Generation: Over a million Mexicans entered from 1900—1930.
		+ Displacement caused by American imperialistic and Capitalistic desires and needs.
		+ Once over the border, get labeled as "dirty Mexicans"

**"Chicano"**:
- Introduced by Immigrant Generation, appropriated by U.S.-born Mexican-Americans.
- Represented countercultural identity formation, rejection of mainstream US Mexican-American cultures.
- Then reappropriated by the Chicano Generation to represent its own counterculture and rebellion against integration, assimilation, and Americanization.
	* The right and power to name themselves Chicanos.
