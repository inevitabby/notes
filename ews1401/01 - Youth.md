---
title: "Youth, Education, and Power"
---

**For Historically-Marginalized Communities...**
- What happens today isn't isolated from the past.
- History must be remembered.
- The effects cannot be erased.

<!--What do you know about the Chicano, Black, Boricua, Asian American, and Indigenous Power Movements?-->

Larry Itliong spoke with Cesar Chaves for a week, Filipinos and Mexicans banded together and had a strike on September 17.
- Strike took 5 years.
- Filipino Farm Labor Union x UFW

<!--
Reflect on your education.
- What was your experience?
- What was the ethnic/racial make-up of the school?

Do you think your elementary, middle, or high school was/is segregated?

DO you think the UC/CSU system practices segregation or exclusion?

Do you think Cal Poly Pomona practices segregation or exclusion?
-->

**Still Falling Through the Crack**: Educational Pipeline
- Study by UCLA Chicano Studies Research Center
- UCLA:
	* 1968: 50 students of Mexican-descent were enrolled.
	* 2020: 6,800 students are Mexican-American and Central American.
		+ Total: 33,000 undergraduate students.
		+ This is much lower than LA's population would lead you to believe. 

**Low-Resources/Surveillance**: Low-income or immigrant schools are usually low-resourced and under surveillance. 

**Criminalization**: Schools are often the site of the first interaction between Black and Brown students and law enforcement.
- This experience can be traumatic.

**Mexican Education by Alejandro Jimenez**: Poet, Educator, and Writor
- 
