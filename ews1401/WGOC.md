---
title: "White Guys on Campus: Chapter 3"
---

# Introduction

## Theme

**Theme**: White men sincerely believe they are the "true" targets of discrimination.
- aka: Reverse Racism

## Framing Reverse Racism

**How-to Frame Reverse Racism**:
1. Campus Racial Diversity
	- Political correctness is a censorship of White people.
2. Race-Conscious Social Policies
	- Affirmative action is reverse racism.[^fn1]

[^fn1]: This ties into White immunity from seeing the effects of racism.

## Key Terms

> ## Key Terms
> 
> **White Immunity**: White immunity means White men don't see the racism minorities see.
> 
> **White Narcissism**: Centering racial experiences on the experiences on White people who are immune to the racism minorities see leads to White narcissism.
> - *e.g., Framing racism as minor isolated instances rather than a systemic issue.
> - An effect of White immunity.
> 
> **White fragility**: When Whiteness becomes fragile, instead of engaging in a multicultural society, it retreats to the myth of reverse racism.
<!--*-->

# The Mythology of Reverse Racism

**The Mythology of Reverse Racism**: *These myths are made possible by (1) ignoring the plight of racial minorities (White immunity) and (2) reframing discussions of racism on White people (White narcissism)*

## Racial Sins

**Racial Sins**: One way to reject *race-consciousness*[^fn2] is to focus on the narrative that White people are being held responsible for their ancestor's actions.
* The racial sins narrative relies on White immunity to ignore real racism and White narcissism to focus on racism insofar as it affects White people.

[^fn2]: **Race-Consciousness**: Policies that aim to improve the conditions of racial minorities

## Minority Privilege

**Minority Priviledge**: The view that racial minorities are systematically advantaged rather than disadvantaged.   
* Contemporary Example: "Barack Obama wouldn't have won if he was White", is a complete reversal of the reality that racism hinders opportunities for non-White people.
* Even More Contemporary Example: "We live in a post-racial society because we elected a Black president"
	+ (Completely ignores the racism before, during, and after the Obama presidency)[^fn3]

[^fn3]: [The racist backlash Obama has faced during his presidency](https://www.washingtonpost.com/graphics/national/obama-legacy/racial-backlash-against-the-president.html)
## Political Correctness

**Political Correctness**: The feeling that White men's views on race and racism were seen as invalid due to their Whiteness.
* *I've seen this in my friend groups with White people saying they feel like they're "walking on eggshells" around minorities.*

## It's Racist to Label Racism

**It's Racist to Label Racism**: Conflating being labeled a racist as being a form of racism is like the endgame of reverse racism because it's basically a get out of jail free card.

## Race-Conscious Policies are Racist 

**Race-Conscious Social Policies are Racist**: The view that race-conscious social policies are the most common form of contemporary racism.
* An example of using **color-blindness** to ignore the uncomfortable reality of racism or privilege.
* Based in the misunderstanding that anything race-conscious is racist.
	+ e.g., racial/ethnic student organizations are the primary source of racism on campus, removing race/ethnicity from college applications will make racism disappear, historically Black colleges and Universities are racist.
* Equal opportunity for me, but not for them.
	+ Ignoring the reality of racism lets White men make arguments against affirmative action.

# Racial Paranoia

- All the mythologies of reverse racism disrupt the invisibility—the privileged defaultness—of being White.
	* The defensive reaction is a demonstration of **White fragility**.
- Poor understandings of racism lead to White guys categorizing people into good (non-racist) and bad (racist).
	* Rejection of social context lets them individualize systemic oppression outside themselves.
- Fear of falling down the racial hierarchy motivates racial paranoia.

<center>
<b>White men's imagined oppression becomes a basis for actual oppression of PoC.</b>
</center>

