# CS1300: Discrete Structures 

The following are my class notes for Discrete Structures.

# Topics

1. [Formal Logic](01 - Formal Logic.html)
2. [Propositional Logic](02 - Propositional Logic.html)
3. [Predicate Logic](03 - Predicate Logic.html)
4. [Proof Techniques](04 - Proof Techniques.html)
5. [Induction](05 - Induction.html)
6. [Recursive Sequences and Recurrence Relations](06 - Recursive Sequences and Recurrence Relations.html)
7. [Set Theory](07 - Set Theory.html)
8. [Combinatorics](08 - Combinatorics.html)
9. [Relations](09 - Relations.html)
10. [Functions](10 - Functions.html)
11. [Graphs](11 - Graphs.html)
