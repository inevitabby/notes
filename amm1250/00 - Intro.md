---
title: "Introduction"
---

# Race & Ethnicity

**Race**: Any one of the groups that humans are often divided into based on [physical traits]{.underline} regarded as common among people of shared ancestry.
- *e.g., bone structure, skin, hair, eye color*

**Ethnicity**: A particular ethnic affiliation or group.
- Based on [perceived shared attributes]{.underline}, not biological factors.
- Ethnic membership tends to be defined by a shared cultural heritage, ancestry, origin myth, history, homeland, language, dialect, religion, mythology, folklore, ritual, cuisine, dressing style, art, or physical appearance.

**Ethnic**:  Of or relating to large groups of people classed according to common racial, national, tribal, religious, linguistic, or [cultural]{.underline} origin or background.

> **Example**:
> - Asian (Race), Chinese (Ethnicity)

> **Remember**: Ethnicity is an [anthropological]{.underline} term.


**Biological Race is a Myth**: Race is primarily a social construct.
- **Genetic Diversity**: Genetic differences between individuals within a "race" group are often greater than the differences between individuals from different "race" groups.
- **Not Isolated**: Physical variations among humans do not fall into distinct, biological defined categories.

> **On Self-Identity**:
> - Race and ethnicity are both self-identifiable.
>	- On the US Census, you can check any box, there is no race or ethnicity police.

> **Example**:
> - Attendants of a Lunar New Year party aren't bound more by *heritage and culture* than some intrinsic biological traits.

# US Standards on Race & Ethnicity

> "Race & Ethnicity are sociopolitical constructs and are not an attempt to define race and ethnicity biologically or genetically"
> — Statistical Policy Directive No. 15, Office of Management and Budget Standards (1997)

In other words, race & ethnicity are based on self-identification to the US government.

> **Classification Standards**: You can see the growth of racial and ethnic categories in the US census. 
> - The 1790 US census had three categories: "Slaves", "Free White Females & Males", and "All Other Free Persons"
> - In 1930, "Mexican" was added as a category, disappeared for 20 years, and was re-added as "Origin or Descent: Mexican; Puerto Rican, Cuban, Central or South American, Other Spanish" in the 70s.

In 2010, race and ethnicity were two separate questions.

In 2024, race and ethnicity were combined into one question. ("What is your race and/or ethnicity?")

> SPD 15: Detailed Categories:
> 1. Asian
> 	- Chinese, Asian Indian, Filipino, Vietnamese, Korean, and Japanese
> 2. Black or African American
> 	- African American, Jamaican, Haitian, Nigerian, Ethiopian, Somali
> 3. Hispanic or Latino
> 	- Mexican, Puerto Rican, Salvadoran, Cuban, Dominican, Guatemalan
> 4. Middle Eastern or North African
> 	- Lebanese, Iranian, Egyptian, Syrian, Iraqi, Israeli
> 5. Native Hawaiian or Pacific Islander
> 	- Native Hawaiian, Samoan, Chamorro, Tongan, Fijian, Mashallese
> 6. White
> 	- English, German, Irish, Italian, Polish, Scottish

> **On Reading Data on Race and Ethnicity**:
> - Be aware of whether categories are alone or in combination.

# Nationality v.s. Citizenship

**Citizenship**: Which country grants the person full [political rights and obligations]{.underline}.
- Legal or political status

**Nationality**: The status of [belonging to]{.underline} a particular nation.
- Ethnic or racial status

> **Citizenship v.s. Nationality**:
> - Citizenship can fluctuate, but nationality is innate and not changeable.

# Biracial & Multiracial Identity

Biracial & Multiracial Identity

Panethnicity

# Culture

**Culture**: Shared meanings, rituals, norms, and traditions among the members of a society.
- *Passed down* from generation to generation
- A distinctive and *learned* mode of living
- Continuously *interacting* and responding to environmental stimuli.
- Both material and non-material.

> **Examples of Material v.s. Non-Material**:
> 
> 1. Material Culture:
> 	- Monuments
> 	- Coin
> 	- Food
> 	- National Flag
> 	- Temple
> 	- Shopping Mall
> 	- Weapon
> 2. Non-Material:
> 	- Social Organization
> 	- Language
> 	- Customs and Traditions
> 	- Religion
> 	- Literature
> 	- Forms of Government
> 	- Economic Systems

# Cultural Norms

**Norms**: Unwritten rules of [acceptable and unacceptable behavior]{.underline} in a culture.
- **Enacted Norms**: Explicit rues
	* *e.g., green light means "go"*
- **Crescive Norms**: Learned and practiced by members of a social unit, but [may not be readily recognized]{.underline} by nonmembers.
	* **Conventions**: Norms regarding the conduct of everyday life.
	* **Customs**: Practices handed down from the past, slow to change.

## Levels of Norms

| Name     | Scope                                  | Sanction of Violations   |
|----------|----------------------------------------|--------------------------|
| Folkways | Daily routines in practice             | None                     |
| Mores    | Defines moral behavior                 | Disapproval              |
| Taboos   | Defines strict prohibition of behavior | Expulsion from the group |
| Laws     | Formally inscribes norms               | Police                   |

> **Examples**
> - I drink coffee and have pancakes for breakfast every day: Folkway
> - Gossiping about people: More
> - You shall not say slurs in public: Taboo
> - Not paying taxes and/or killing someone: Laws

# Consumer Research Design

**Primary Research**: We gather the data ourselves.
- Exploratory Research: We have a theory we want to prove.
- Descriptive Research
- Casual Research

**Secondary Research**: We analyze existing data.
- Internal Sources
- External Sources

# Interview

Types & Examples of Interviews:
1. Structured: Researchers ask the same questions and the same blueprint for responses.
	- e.g., demographic interview
2. Semi-Structured: Researchers ask predetermined questions and unplanned questions.
	- e.g., job interview
3. Unstructured: No questions are predetermined.
	- e.g., discovery interview

**Focus Group Interview**: Small groups of research participants (5—10) interact with interview and one another.
- Sales figures and polling were popular in the 90s, but focus groups were a qualitative research form.
	* Rather than providing definite numbers, focus groups provide deep understanding of consumer habits.
- e.g., Focus groups led to the creation of the Barbie doll, revealed that women have lots of influence over what car families buy, etc.

**Questionnaire & Survey**: Cheap and easy way to get larger database.
- You may not reach potential customers only doing email blasts.

**Longitudinal v.s. Cross-Sectional**:
- Longitudinal: Track responses of the same sample to detect changes over time.
- Cross-Sectional: Examine different samples at one point in time.

**Experiment**: Test hypotheses by experimentation.

**Surveys Used to Study the US**:
- Employment: Current Population Survey
- Crime: National Crime Victimization Survey
- Health: National Health Interview Survey
- Election: American National Election Studies

# Consumer Research

## Market Segmentation

**Demographic**
- age
- years of education
- income
- family size
- gender
- race
- marital status

**Geographic**
- rural/urban
- climate
- radius
- neighborhood
- nearby resources and amenities

**Psychographic**
- activities
- interests and opinions
- personality attitudes

**Behavioral**
- purchasing history
- usage habits
- brand loyalty
- purchase reasoning

## Model of Consumer Behavior

$$
\text{Marketing and Other Stimuli} \to
\text{Buyer's Black Box} \to
\text{Buyer's Response
$$

We want to unravel the buyer's black box.

## Factors Influencing Consumer Behavior

1. Consumer Culture
	- *My friends have it...*
2. Psychological Core
	- *I have the time...*
3. Decision-Making
	- *I'm willing to buy this*

## Decision-Making Process

1. Need Recognition
	- My fridge sucks
2. Search for Information
	- Looking online
3. Evaluation of Alternatives
	- Comparing models
4. Purchase Decision
	- Buying the thing
5. Post-Purchase Evaluation
	- Did it work?

# How Age Cohorts Impact Consumption

## Market Segmentation

- Age
- Marital Status
- Stage in Family Life CYcle

## Age and Consumer Identity

Consumers undergo predictable changes in their values, lifestyles, and consumption patterns as they age.
- Marketing is targeted at specific age groups.

Ranges:
- Children: 0—14
- Teen: 13—19
- Adults: 18—34
- Seniors: 65+

### Age Cohort

**Age Cohort**: Set of people born in the same short interval.
- Consumers in the same age cohort experience the same economic, social, political, and environmental events.

> **Zeitgeist**: The spirit of the times.
> - e.g., "The free love of the 1960s"

## Generations

A generation typically spans 15—18 years.
- We use big events to divide generations.

Discuss: Find one person from our given generation. 

Current Facts:
- Right now, Generation X are the dominant generation in the US
- Baby Boomers are fading.

The Silent Generation (1928 - 1945): 
- Influences
	* Polticial instability, civil rights movement
	* Great Depressions, WWII
- Work: Work is a must
- Career: Slow and Steady

Baby Boomers (1946 - 1964):
- Influences
	* Postwar prosperity, TV
	* Moon landing, watergate, Vietnam
- Work: Work is expected
- Career: Upward mobility.
- Prefer shop to store

Generation X (1965 - 1980):
- Influences
	* MTV, Nintendo, PC
	* Shaky economic times, Great Recession
- Work: Work is expected
- Career: Upward mobility
- Viewing online content on laptops

Generation Y (1981 - 1996):
- Influences
	* Digital and mobile technology, social media
	* Great Recession
- Work: Work is a means to an end
- Career: Switch path frequently and fast.
- Purchase based on photos

Generation Z (1997 - 2012):
- Influences
	* Economic downturn
	* Covid-19
- Work: Work is consistently evolving
- Career: Career multi-taskers.
- Short online attention spans.

Generation Alpha (2013 - 2025):
- Influences
	* COVID-19, AI
	* to be seen.
- Work: ???
- Career: ???

## Consumption by Age Segmentation

Youth Market: Often represents rebellion

Senior Market: Retired


## Generation Research

- Boundaries are imprecise, not scientifically defined.
- Labels can lead to stereotypes.
- Conventional views of generations are skewed towards the experience of the upper middle class.

# Sex and Gender

Sex: Biological

Gender: Socially constructed

## Sex Ratio

**Sex Ratio**: Ratio between males and females in a society.
- Natural sex ratio at birth is slightly biased towards males (103:106)

> **Random Life Expectancy Facts**:
> - Infant and child mortality rates higher for boys.
> - Higher life expectancy at birth for infant girls.

## Gender Identity

**Sex Orientation**: A during pattern of emotional, romantic, and/or sexual attraction to people of a particular gender.
- Formed as teens or young adults.
- e.g., hetero, bisexual, homosexual, pansexual

**Gender Identity**: Person's psychological sense of their gender
- Formed by age 3
- e.g., cis, trans, non-binary, gender-fluid

## Gender Differences

- Biological: Biology created gender roles, makeup, and gender stereotypes.
- Evolutionary: Evolution creates gender differences somehow.
- Cultural: Culture creates gender differences
- Religious: God decided what masculine and feminine roles are.
- Social Role Theory: Widely-shared gender stereotypes develop from the gender divisor of labor that characterises a society.

## Gender Roles

Social Construction of Gender: Gender roles are socially constructed. Differences are created or exaggerated.

**Sexism**: Ideology that one sex is superior to the other.

**Gender Stereotypes**: Widely held believe or generalization about the behaviors and characteristics attributed to women and men.
- *e.g., women are caretakers, men don't cry, etc.*

**Glass Ceiling**: Invisible barrier that blocks promotion of a qualified individual due to gender, race, or ethnicity.

## Gender Pronouns

**Gender Pronouns**: Terms people choose to refer to themselves that reflect their gender identity.

**Gender Neutral (Inclusive) Pronoun**: A pronoun that doesn't associate a gender with the individual being referred to.

**Bias-Free Language for Gender**:

| Problematic  | Preferred           |
|--------------|---------------------|
| Man          | People              |
| Mankind      | Human Beings        |
| Manpower     | Work Force          |
| He or she    | They                |
| His or her   | Their               |
| Mothering    | Parenting/Nurturing |
| Housewife    | Homemaker           |
| Waitress     | Server              |
| Salesmanship | Selling ability     |

> Key Points: Don't imply only mothers care for children or people in a role are of a particular gender.

## Feminist Movement

First-Wave: 

Second Wave:

Third Wave:
- Women of all races, classes, and cultures.
- Financial, social, and cultural inequalities
- Individualism and diversity, abolishing gender-role stereotypes

Fourth Wave: 
- Justice against assault and harassment.
- Equal pay for equal work.
- Bodily autonomy

## Gender Gap

Women are underrepresented in every section of society.
- Politics
- Corporate America
- Education
- Marketplace

The reference person is a White male in his 30s who weighs 155 lbs.
- Pianos assume a hand span of 22.9 cm for men for octaves.
	* (For women would be 20.3 cm)
- Female car crash test dummies weren't required until 2011
- Temperature regulation in office buildings is set for 155 lb men.
- Voice recognition works best for males.
