---
title: "Final Deliverables"
---

# Final Presentations

**Final Presentations**:
- 10 minutes.
- Business attire
- Pick the best speaker in the team
- Rehearse and time it
- Approach this as selling the product
	* Product demo should be a good part

> Notes:
> - Professor dislikes when everyone talks
> - Good Trick: Every slide ends up with a question that gets question in the next slide.

# Final Report

**Final Report**:
- Outline:
	1. Introduction
	2. Problem Description
	3. Solution Approach
	4. Design Architecture Description
	5. Implementation Discussion
	6. Testing Approach
	7. Conclusions and Suggestions for Future Work
	8. Appendices

> **Optimal Page Count**: 50—100 

# CEO Report

**CEO Deliverables**:
- CEO Report:
	* Explain every member's contributions.
	* Show breakdown of source code contributions.
- Slides from Final Presentation
