---
title: "Introduction"
---

**Reference Book**: *Software Engineering: A Practitioner's Approach*, 5th ed

**Professor's Approach**: Software Engineering is a~~n~~ ~~art~~ *discipline with methodology and rules*.

**Semester-Long Web Project**: 
- We'll explore backend, frontend, and middleware.
- Group project (5 people).
- Thousands of LOC.
- We'll be expected to hit certain milestones each Friday.
