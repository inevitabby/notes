# CS3110: Formal Languages and Automata

The following are my class notes for Formal Languages and Automata.

# Topics

1. [Chapter 0 (Review)](00 - Intro.html)
2. [DFA](01.html)
2. [NFA](02.html)
3. [Regular Expressions](03.html)
