---
title: Prelude
---

> **Motive**: Studying MIPS improves understanding of all assembly languages.
> - *Metaphor: You don't re-learn how to drive every time you buy a new car.*

<!--
**Architecture**: Logical description of a computer's basic components and operations.
- Unique to each processor family.
- Each architecture has its own assembly language.

**Machine Cycle**: One machine cycle results in the execution of one machine instruction.
- *(Fetch-Decode-Execute cycle)*

**Register**: Part of the processor than can hold a bit pattern representing some data.

> **Note**: Enhanced assembly language includes pseudoinstructions that make life easier.

**Machine Instruction**: Pattern of bits that corresponds to a fundamental operation of the processor.
- Addition and subtraction can be performed in one machine cycle, but loops and branches alter the cycle.
- On MIPS32, each instruction is 32 bits long.

> **Example**: A machine instruction
> - MIPS Assembly: `add $t0,$t1,$t2`{.mips}
> - Machine Instruction: `0000 0001 0010 1011 1000 0000 0010 0000`
>
> *(Translation of MIPS assembly $\to$ machine instructions is done by an **assembler**)*
-->


