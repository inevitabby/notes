# STA2260: Probability and Statistics for Computer Scientists and Engineers 

The following are my class notes for Probability and Statistics for Computer Scientists and Engineers.

- [Introduction](01.html)
- [Descriptive Statistics](02.html)
- [Probability](03.html)
