---
title: "Statistical Inferences"
---

# Confidence Intervals

**Estimation**: Estimating or predicting the value of unknown population parameters using the value of a sample statistic.

**Estimator**: The sample statistic used to estimate an unknown population parameter.

**Estimate**: The value assigned to an unknown population parameter based on the value of a sample statistic.

**Point Estimate**: A single numerical value to estimate a population parameter. 

**Interval Estimate**: Interval around the point estimate likely to contain the corresponding population parameter.

**Example**: Estimation

$H_1: \mu \ne c$: Reject if $|T| > t_{\alpha / 2 , n - 1}$

$H_1: \mu > c$: Reject if $T > t_{\alpha , n - 1}$

$H_1: \mu < c$: Reject if $T < - t_{\alpha , n - 1}$

