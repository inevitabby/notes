---
title: "Continuous Random Variables"
---

$X$ is an absolutely continuous random variable if there exists a nonnegative function $f$, defined for all real $x \in (-inf , \inf)$, having the property that for any set { $x \le a$ } of real numbers,

$$
P(x \le a) = \int_{ \{ x \le a \} } f(x) dx
$$

The function $f$ is called the probability density function of the random variable $X$.


