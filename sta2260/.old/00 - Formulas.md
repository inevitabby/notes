**Median**: Middle number

**Mode**: Number that occurs most-frequently.

**Range**: Max - Min

**Quartiles**: Split distribution into 4 even parts.
- $Q_1 = 25th$
- $Q_2 = 50th$
- $Q_3 = 75th$

**IQR**: $Q_3 - Q_1$

**Sample Mean** / **Average** / **Arithmetic Mean**:

$$
\bar{x} = \frac{ \sum x }{n}
$$

**Sample v.s. Population**:
- **Sample**: Subset of the population.
	* $\bar{x}$ (sample mean)
	* $n$ (sample size)
- **Population**: Thing we want an insight into.
	* $\mu$ (population mean)
	* $N$ (population size)

Standard Deviation of the Sample:

$$
s = \sqrt{ \frac{1}{n-1} ( \sum_{i=1}^n x_i^2 - n \bar{x}^2 ) }
$$

Standard Deviation of the Population

$$
\theta = 
$$
