---
title: "The Vocal Workout"
---

# Types of Exercises

A. **Preliminaries**: Preparing/exploring/releasing/connecting to your body.
B. **Pitch**: Habitual degree of highness or lowness.
C. **Resonance**: Colour/tone of voice.
D. **Intonation**: Expressive movement of voice in relation to conveying meaning.
E. **Voice Quality**: Exploring various useful voice qualities.

> **On Exercises**:
> - Most, if not all, exercises offer more than one learning opportunity.
>	- *e.g., pitch may be the primary goal, but the secondary focus could be intonation.*
> - Always know the goal of an exercise.
>	- 'Because I was told to do it' shouldn't be your reasoning.

**Hierarchical Approach**:
1. Establish *healthy, clean vocal tone* that can be sustained before building *consistency and stamina*.
	- Pitch exercises establish a modified note, resonance balances the tone.
	- Regular exercise build control, stamina, and strength. 
2. Skills acquisition moves from simple to more complex tasks.
	- Regular practice is important.
	- After mastering exercise, apply those vocal abilities to more demanding speaking and reading tasks.
		* Multitasking like this will eventually make it automatic.

> **On Limitations**:
> - These exercises can be done solo and are chosen for clinical effectiveness, but a therapist may recommend innervations that require facilitation.

> Remember the golden rules!
> 
> **Golden Rules**:
> - Spirit of play: Try things out.
> - Self-awareness: Develop awareness without judging.
> - Sensing: How does it feel? What do you hear?
> - Curiosity: Be interested rather than critical.
> - Time: Practice often and little.
> - Baby steps
> - Journaling: Externalize observations and hunches.
> - Demands and capacities: Big changes don't happen quickly.
> - Enjoy: Have fun.


