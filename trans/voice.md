---
title: "Voice Feminization"
---

These are my notes on *The Voice Book for Trans and Non Binary People* by Matthew Mills and Gillie Stoneham.

# Contents

1. [Introduction](intro.html)
2. [Anatomy and Physiology of Sound](anatomy.html)
3. [The Vocal Workout](workout.html)

