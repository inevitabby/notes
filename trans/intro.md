---
title: "Introduction"
---

# Solution-Focused Therapy

**Solution-Focused (SF) Therapy**: Future focused and goal-directed voice therapy.
- Supported by many speech and language therapists for voice change.
- "Lead from behind", assumes you're the expert in your life and what needs to change.

**On Exercises**: Voice is a set of behaviours that changes by doing.
- Knowledge *and* practice are important.

**On Mindset**: Voice feminization is about developing the voice you have.
- Knowing your voice is important.
	- Cringing at the discord between your internal and external voice is natural, but don't hate yourself.
	- Emphasizing strengths can help keep motivation.
- Falling in love with your voice is a process.
	* Respect the unknown potential of your voice as it is *right now*.

**Golden Rules**:
- Spirit of play: Try things out.
- Self-awareness: Develop awareness without judging.
- Sensing: How does it feel? What do you hear?
- Curiosity: Be interested rather than critical.
- Time: Practice often and little.
- Baby steps
- Journaling: Externalize observations and hunches.
- Demands and capacities: Big changes don't happen quickly.
- Enjoy: Have fun.

**Focus in, Focus out**: Anxiously focusing away yourself is detrimental because it prioritizes the listener's response over your vocal sensation, effort, and relationship to yourself. This order of focus is helpful:
1. *Focus in* and pay attention to the *sensation* of your voice production and the *memory* of your target pitch.
2. *Acknowledge you vulnerability* in speaking up and finding courage to participate.
3. *Connect* with your communicative intention and what's important to you.
4. *Focus out* on your listener with social communication behaviours, staying true to your message and what you want to communicate.

> **More on Focus in, Focus Out**: Training to focus on intention by deconstructing the individual parts like this makes it easier to put it all together.

# Challenges

> *"Young people might behave as they hope to be rather than how they actually are – it comes with age and worrying less about what people think."* \
> \ — *Grace, The Voice Book for Trans and Non Binary People (Ch 2)*

**Paradox of Change**: "Why does change need to happen, and who is the change for?"
- To have a voice flexible enough to be communicative, you need to explore your communicative potential rather than reject how you sound today.

**Care About Your Present Voice**: Communication requires vulnerability and is made of millions of tiny things between you and another person.
- Care more about exploring other people with genuine curiosity than what they think of you.

> **More on Play**: "I don't want to explore or 'accept my voice' — that's why I'm here"
> - Playing with your range lets you find the spots that are good for natural conversation and builds awareness of what your voice can do.
> - This is especially important for avoiding stereotypical gender cues.

**Authenticity**: Authenticity comes from making small changes over time and believing in yourself.
- *Disguising* the voice *(e.g., speaking softly, not speaking, etc.)* reinforces that your voice and identity is not okay.
- A comfortable, authentic, easily produced sound will not sound phoney.
- Finding a good pitch range is important, avoiding lower pitches can result in unnatural intonations.

> **On Camp**: Finding direct vocal tone, commitment to your words, balance between pitch and resonance, and muscular articulation will avoid sounding camp.
> - Avoid "gripping" the larynx, voice should be comfortable.

**New Voice, Old Friends**: Be bold and hold your note unequivocally.
- Like a new haircut, they will get used to it.

# What is Communication?

> *"It’s not just the voice, it’s the whole way you present yourself, and the way you talk and the way you empathise with people and understand them, and the eye contact and the smiling and everything."* \
> \ — *Ruth, The Voice Book for Trans and Non Binary People (Ch 2)*

**Key Parts of Communication**:
1. **Verbal Communication**: Spoken language
	- Different purposes/contexts require difference vocabulary and sentences.
	- Conversation flow, asking questions, managing interruptions.
2. **Non-Verbal Communication**: Body language
	- Helps illustrate what we mean and adds emotional content.
	- Eye contact, facial expression, gesture, posture, etc.
3. **Paralanguage**: Vocal signals accompanying speech but not directly related to language.
	- Pitch, intonation, vocal tone and quality, rate of speech and loudness, etc.

> **In Short**: Every behaviour is a form of communication, not just speech.
> - Emotions can leak through non-verbal behaviours.
> - Non-verbal overrides verbal when there's incongruence.
>	- e.g., confident speech and anxious non-verbal behaviours

**Gender Cues**: Some communication behaviours have a social history of carrying certain gender cues.
- To avoid adhering to rigid stereotypes, play/explore cues to find your natural version.

> **On Neuro-typicality and Neuro-difference**
> 
> Trans and non-binary individuals on the autism spectrum can have greater difficulties learning the social expectations of their gender.
> 
> It's good to examine the *fundamental social communication rules* and make a positive choice on whether taking them fits your personal notion of good communication, like:
> 
> - Being aware of eye contact and length of gaze with communication partners.
> - Turn-taking and noticing when other people are seeking to join the conversation.
> - Engaging in collaborative conversation.
> - Looking after yourself and asking for clarification when needed.
> - Keeping your body language and gestures relaxed and natural in a way that enhances the verbal message.

# Confidence

**Confidence** is frequently cited in narratives and discussions on progress.

It's often said that developing voice and communication skills is "*being* more confident", though it's more accurate to say you are *feeling* more confident.

If you *feel* more confident, try to identify what you are *doing* that's causing it.
- This is important for building *self-efficacy*.

> **On Self-Efficacy**: Self-efficacy is the belief that we have the ability to carry out a task successfully.
> - This impacts instills more confidence in speaking situations.

# Importance of Breathing

**Shared Life Experience**: Breathing connects us to other people.
- Breath affects the whole body.
- We breathe in a *shared rhythm* when we're with other people.
- Developing freedom in breathing results in a voice that is *more responsive*.

**Different Activities**: Breathing is a reflex, but we need to relearn how to breathe freely. 
- The types of breathing we do at rest, while performing physical activity, and speaking are different.
- We override the automatic breathing reflex for our communicative needs.
	* *Breath support* is effective breathing for speaking.

**Mindful Breath**: While our minds can wander ahead, our body is only in the present moment.
- Connecting your mind to your breath helps with the focus-in focus-out framework, stammering, and exploring your voice.
- *e.g., to express an emotion, you need to feel it, not just speak it*


