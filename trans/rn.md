---
title: "Conversation with a Registered Nurse"
---

# Preface

These are snippets of a short verbal conversation I had in 2024 with a registered nurse, mainly about HRT for transgender women.

This is an unprofessional, firsthand account of a conversation about [one person's]{.underline} knowledge; **this is not medical advice**.

> **On Format**: My additions and comments are indicated via footnotes.

# Pills, Patches, and Injections

**Me**: What's the difference between them?

**RN**: There is not enough science to show one method as better than another.

Pills and patches result in more stable e levels than injections. For injections, e levels steadily decrease through the week. However, they are more affordable per dose.[^fn5]

[^fn5]: Some conflation between dosing and e levels may have happened here. Patches theoretically can provide more consistent dosing, though they might provide less-and-less throughout the week. E levels can be made more stable by injecting more often, et cetera.

There is no conversion from one to another. For example, there is no formula to translate 4 mg of oral pills into $X$ mg/ml of injections, they are all metabolized and processed differently.

On the topic of injections, the doses are often too high, so I tend to give smaller doses which can make some patients feel like they're losing something, but it reduces risks of side effects.

# On Stability

**Me**: What effect does e stability have on transition?

**RN**: For some, mood and energy stability. Cisgender women's levels fluctuate throughout the week too.

# Buccal

**Me**: Some of my friends take their pills bucally, why?

**RN**: Since oral pills are metabolized by the liver when you swallow, it was thought that taking pills bucally to avoid the first-pass metabolism by the liver would reduce strain on the liver.

However, studies showed that there was no greater risk between buccal and swallowing on cisgender women. Taking pills bucally also makes doing bloodwork harder because it exaggerates the peaks and falls in e levels, so we don't recommend it anymore; but other providers still might.

# Anti Androgens

**Me**: Is there an anti-androgen that doesn't suck? What about Bica?

**RN**: Bicalutamide is used in Europe, but not in the US due to long-term safety concerns.[^fn1]

[^fn1]: Bica works by blocking androgen receptors rather than production like spiro, and there are concerns about long-term liver toxicity. 

Spironolactone is only *true* anti-androgen we have that limits the production of testosterone. We do bloodwork every 6 months in that case.

Finasteride is another option that inhibits the conversion of testosterone to DHT, a *far* stronger androgen. Testosterone levels won't go down, but there's no toxicity concerns because it doesn't affect the liver like spironolactone.[^fn3] It can be taken with spironolactone.

[^fn3]: It also apparently does things for male pattern baldness.

For some, estrogen without anti-androgens could be enough for them to reach their transition goals.

# Surgery

**Me**: What about surgery?

**RN**: It takes about 2 years for most of the feminizing effects to reach their peak, so 2 years is a good time to evaluate whether surgery is a good choice.[^fn8]

[^fn8]: It looks like the 2-year mark is a general recommendation for genital surgery, which is quite rare. FFS and breast augmentation might be considered earlier if needed, though I agree that waiting is definitely safer.

I recommend anyone to wait it out—and who knows—HRT alone might achieve transition goals.
