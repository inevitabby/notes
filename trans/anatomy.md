---
title: "Anatomy and Physiology of Sound"
---

# Estill Model Overview

**Power-Source-Filter Model of Voice**:
1. **Power**: Breath involving the lungs, ribs, diaphragm, and abdominal muscles to propel the breath into the vocal tract.
	- *System: Respiration*
2. **Source**: The onset of voice using movement of the vocal folds (larynx).
	- *System: Phonation*
3. **Filter**: The vocal tract's modification of sound as it passes through  .
	- *System: Resonance and amplification*

![](.images/doodle_00.png)

# A. Power (Breath)

We *breathe in* and *speak out*.
- More power into breath results in a less "clean" and efficient vocal note.
	* Pumping too much into the larynx can contribute to vocal fatigue.

Breathing engages the intercostal muscles between the ribs and your abdomen—

> Inhale:
> * Diaphragm flattens
> * Rib cage expands
> 
> Exhale:
> * Diaphragm raises
> * Rib cage contracts

—so it is important to move you abdomen and chest correctly:

> *"Imagine breathing inwards to inflate a pool noodle around your waist"*
> \ *— My friend's drama teacher*

1. Your belly should expand and move forward a little on the in-breath.
	* Tightness in your abdomen causes tightness in the larynx and voice.
2. Abdominal muscles should *support* the out-breath. 
	* Strong/weak pressure impacts the larynx and voice.

# B. Source (Larynx)

> Following the Path:
> 1. The outward breath goes from the lungs up the trachea and into the larynx.
> 2. The true vocal folds of the larynx act like a valve and draw together, fluttering rapidly and vibrating the air to make sound.
> 	- The vibrating folds themselves also add their own "harmonic" frequencies to the voice.

Functions of the Larynx:
- Protect the airway: The vocal folds can close rapidly to prevent choking.
- Lets us build air pressure for effortful tasks.
- Controls flow of air in and out of the lungs.
- The *source of sound*.

> **On Aging**:
> 
> The vocal folds are made of skin, gel, ligament, and muscle.
> - Hydration affects the gel and is why voice quality changes throughout the day.
> - With age, the gel layers and muscle fibers thin (atrophy).
>	- Masculine voices age towards higher octaves.
>	- Feminine voices age towards lower octaves.

## Pitch

**Pitch**: The degree of highness or lowness in the perception of sound.
- Controlled by the speed the vocal folds are vibrating.
- Interrelated to resonance and expressive intonation.

> **Modal v.s. Average Pitch**:
> - Modal: Resting pitch, the pitch you return to.
> - Average: The average pitch you've spoken in, changes with emotion / intonation.

> **Ballpark Ranges**:
> - Cis Male: 100—140 Hz
> - Recommended Ranges for Trans Women: 165—196 Hz

**Notes to start feminising pitch exercises**:
- E3 (165 Hz)
- F3 (175 Hz)
- G3 (196 Hz)
- A3 (220 Hz) (rare)

> **Measuring Pitch**
> - [Audio Spectruym Analyzer (Fdroid)](https://f-droid.org/packages/org.woheller69.audio_analyzer_for_android/) is a good tool for measuring your pitch.
>	- It's also helpful to see the contour movement of your voice rising and falling (intonation), loudness, and modal pitch.
> - **Remember**: Focus on the *sound* and *feeling* of your target pitch, don't over-focus on the measurement.

> **On Surgery**: 
> - A last resort for *after* you've achieved maximum gains from voice therapy.
>	- Phonosurgery only affects pitch, which is only one facet of voice.
> - Risks: Irreversible voice deepening, variable outcomes, deterioration in voice quality, loss of upper singing range, long-term pitch instability.

## Intonation

**Intonation**: The movement of pitch, used for meaning-making.
- *aka: inflection*
- Changes with accent/dialect and culture.
- Highly individual.

# C. Filter

> Following the Path:
> 1. The vibrating air produced at the larynx travels up the vocal tract,
> 2. Gets amplified in the throat, and
> 3. Leaves through the pharynx (mouth/nose).

**Formants**: The natural resonance of the throat and pharynx modifies the fundamental sound.
- This is known as resonance and intensity (amplification), which gives the voice a richer tone (timbre).
- Affected by length of vocal tract and fine muscle control in the larynx and pharynx.

> *"If you pronounce it as tim-ber instead of tam-ber, I'm going to be disappointed.*
> \ *— My music professor*

> On the Pharynx:
> - Lowering the palate makes air enter the nasal cavity.
> 	* *e.g., 'm', 'n', 'ng' (humming) sounds*
> - Raising the palate makes air enter the oral cavity.

**Secondary Resonance**: Occurs in the bones and muscles.
- Affected by physical makeup and habit.
- This affects the identity/style if a voice
	- e.g., 'warmth' caused how the speaker balances resonance.

> **Getting a Richer Sound:**
> - Any voice becomes richer when the sound resonates freely through the spaces in the body, which is also important for projecting the voice.
> - Identifying and raising resonances  in the mouth and head helps with voice feminization.
>	- Higher and more forwards tongue position
>	- Smaller jaw movements.

## Articulation

Our moving articulators *(tongue, soft palate, lips, jaw)* interact with fixed ones *(hard palate (roof of mouth), ridge behind teeth (aleovar ridge), and teeth)*.

Some studies suggest the women a perceived to speak with more precision and clearer articulation.
- Though, intelligibility is a key part of confident communication.


