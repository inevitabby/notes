<center>
	<h1 id="notes"><a href="https://inevitabby.gitlab.io/notes">Academic Notes</a></h1>
</center>

## Computer Science

- [CS1300: Discrete Structures](cs1300)
- [CS1400: Introduction to Programming and Problem Solving](cs1400)
- [CS2600: Systems Programming](cs2600)
- [CS2400: Data Structures and Advanced Programming](cs2400)
- [CS2640: Computer Organization and Assembly Programming](cs2640)
- [CS3110: Formal Languages and Automata](cs3110)
- [CS3650: Computer Architecture](cs3650)
- [CS4800: Software Engineering](cs4800)

## Mathematics

- [MAT1150: Calculus II](mat1150)
- [STA2260: Statistics](sta2260)

## General Education

- [PHL2020: Critical Thinking](phl2020)
- [PLS2010: Introduction to American Government](pls2010)

## Self-Study

- [CS50: Introduction to Computer Science](cs50)

## Queer

- [Conversation with a Registered Nurse](trans/rn.html)
- [Voice Feminization](trans/voice.html)
- [Planned Parenthood Telehealth HRT Search](trans/pp.html)
