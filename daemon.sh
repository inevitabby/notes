#!/usr/bin/env bash
cd "$(dirname "$0")" || exit
DIRECTORIES=("cs1300" "cs1400" "cs2400" "cs2600" "cs2640" "cs50" "phl2020" "mat1150" "pls2010" "mips" "ffmpeg" "sta2260" "cs3560" "cs4800" "cs3650" "cs3110" "amm1250" "trans")
N=32 # Number of parallel Pandoc processes
BASE_PANDOC_ARGS="-f markdown+lists_without_preceding_blankline --katex --highlight-style=pygments --wrap=preserve --standalone --quiet --template template.html --lua-filter=.util/plantuml.lua"
STYLE_FILE="style.min.css"
RUNNING=true
source .util/convert_markdown.sh
source .util/clean.sh
# Cleanup output files on exit
stty -echo
function cleanup() {
	stty echo
	# Stop conversion loop
	RUNNING=false
	wait
	# Convert all notes
	printf "\e[1;32mCleaning up HTML files...\e[0m\n"
	rm -rf ".md5sums"
	convert
	# Clean up outputted files
	rm -f "public/${STYLE_FILE}"
	for INPUT_DIR in "${DIRECTORIES[@]}"; do
		for HTML_FILE in "public/${INPUT_DIR}/"*.html; do
			clean "${INPUT_DIR}" "${HTML_FILE}"
		done
	done
	wait
	# Deduplicate the analysis style file
	awk -i inplace -f ".util/css-formatter.awk" "public/${STYLE_FILE}" # Place all rules into their own lines
	awk -i inplace '!seen[$0]++' "public/${STYLE_FILE}" # De-duplicate lines
	awk -i inplace '{ printf "%s", $0 }' "public/${STYLE_FILE}" # Remove all newlines
 	exit
}
trap "cleanup" SIGINT
# Convert Markdown to minified HTML
function convert {
	# Convert notes
	for INPUT_DIR in "${DIRECTORIES[@]}"; do
		for INPUT_FILE in "${INPUT_DIR}/"*.md
		do
			((i=i%N)); ((i++==0)) && wait # Limit number of parallel processes
			convert_markdown "${INPUT_FILE}" "${INPUT_DIR}"
			if [ -d "${INPUT_DIR}/.images" ]; then # Copy images if present
				OUTPUT_DIR="./public/${INPUT_DIR}"
				mkdir -p "${OUTPUT_DIR}"
				cp -r "${INPUT_DIR}/.images" "${OUTPUT_DIR}/"
			fi
		done
	done
	# Convert special files
	(pandoc -i "README.md" ${BASE_PANDOC_ARGS} | awk -f ".util/minify.awk") > "public/index.html" &
	(pandoc -i "ABOUT.md" ${BASE_PANDOC_ARGS} --toc | awk -f ".util/minify.awk") > "public/about.html" &
	# Wait for all subprocesses to finish
	wait
}
# Oneshot mode
if [[ $1 == "-o" ]]; then
	cleanup
fi
# Conversion loop
printf "\e[1;32mConversion daemon is running...\e[0m\n"
while $RUNNING; do
	convert
	sleep 1
done
