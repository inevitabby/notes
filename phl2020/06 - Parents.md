# Three Kinds of Loving and Attentive Parents

1. **Authoritarian**: "I make the rules, you'll follow them or face consequences."
	- Children tend to be more passive.
2. **Permissive**: "Whatever the child decides is fine."
	- Children tend to be less disciplined.
3. **Authoritative**: "I know more, here are the guidelines and their reasons; though they can change if you can convince me to."
	- Children tend to be very independent.
	
> Note: Parents who mix authoritarian and permissive are the worst of all three.

4. **Neglectful**: The secret bonus kind of parent you unlock after completing the full game.

