# Quick Review

**Principles of How the Human Mind Works**:
1. Biological Role
2. Conscious vs. Subconscious Levels of Mind
3. Sense-Perception is the Root of Knowledge (5 Senses + Introspection)
4. Language Organizes and Communicates Sense-Perceptions 
5. Emotional/Physical Pleasure & Pain
6. Information & Ideas From Other People

---

**Law of Non-Contradiction**: Nothing can be both $A$ and $\lnot A$ at the same time and in the same respect.
- The only place contradictions can exist is our minds.

---

**Logic**: A way of thinking that is (1) *Rooted in evidence* and (2) *Not self-contradictory*.
- "The art of reasoning" ("art" in the sense that it's an applied science)
- Goal of Logic: To find truth.

---

**Truth**: Mental content that conforms to reality.
- Arises only at the same time as the issue of falsehood.

---

**Objectivity**: Choosing to be committed to facts, logic, and truth
- Prioritizing anything above facts, logic, and truth breaks objectivity
