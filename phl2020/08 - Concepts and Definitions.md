# Definitions

**Definition**: A short statement that tells the essential nature of a reference/concept.
- Complex definitions like "government" require a chain of definitions to understand. Weak chains mean poor understanding.
- Acceptable degrees of precision vary for context.

## Three Functions of a Definition

1. **Paradigm/Prototype**: Explain exactly what things fit in that concept.
2. **Clarify Relation**: Relate concept to other concepts.
	- Aka: ostensive definition ("things like that")
3. **Summary Statement**: Summarize all current knowledge of referents.

## Six Rules of a Good Definition

1. Must have a *genus* and a *differentia*.
	- Tip: If the concept is a noun, the genus will be a noun. If it's a verb, so will the genus; et cetera.
2. Must be *coextensive*.
	- Aka: Must not be *too broad* or *too narrow*.
3. Must be *essential*
	- aka: Must name characteristics that cause or explain most of the other characteristics.
	- Example: "Heart is an organ that goes lub dub" (nonessential) v.s. "Heart is an organ that pumps blood around the body" (essential, and explains the lub dub sound)
4. Must not be circular.
5. Must not use negative terms unnecessarily.
	- Example: "A cat is not a dog"
6. Must not use vague, obscure, or metaphorical language.

## Four Steps of Constructing Definitions

1. **List varied referents**
	- Decide meaning of the word
	- Set questionable referent aside for step 4
2. **Make a diagram with genus at top and concept and other species on the second level**
	- "What does this compare and contrast to?", "What are similar things?"
	- "What groups all of these things together?"
3. **Complete chart by listing differentia for each species**, underlining important ones.
4. **Write definition with genus and differentia**
	- Double-check against the Six Rules of a Good Definition, especially coextensivity.

> **Tip: Indication of Missing Genus (Not Foolproof)**: When the definition begins with "when" or "where".

# Concepts, Genus, and Species

**Concept**: Idea that grasps an open-ended grouping of similar existents.
- "Like a mental file folder"
	* Used for the present, past, and future; knowns and unknowns
- **Four Parts of a Concept**:
	1. **Referents** of the concept
	2. **Word**: A spoken/written/signed *symbol* of the concept
	3. **Mental grasp** of the word in a story
	4. **Definition**: A short statement of the essential nature of the referents.

**Genus and Species**:
- **Genus**: Broader category that has various subcategories.
- **Differentia**: Differentiates subcategories of a genus from each other.
	* *Can rarely be expressed in a single word*
- **Species**: Narrower categories that make up the genus.

> Example of Genus and Differentia: *"Humans are rational animals"*
> - Genus: *Animals*
> - Differentia: *Rational*

## Two Rules of Classifications

1. Principle(s) should be *consistent* and make categories (species) *mutually exclusive* and *jointly exhaustive*.
	- **Consistent**: Don't mix inappropriate principles into one definition.
	- **Mutually Exclusive**: Species shouldn't overlap; a referent should only appear once.
	- **Jointly Exhaustive**: Every referent should be able to fit into a species; nothing should be left out (in this class we fix this by adding an "etc.")
2. Principle(s) should identify *essential attributes* (differentia).
	- *For most manmade objects, the essential attribute is function.*

## Aristotle's Six Top-Level Categories for Existents

Question: "How do we classify every thing in the world?"
- Answer: Aristotle claims that everything that exists can be categorized into the following six top-level categories:

**Aristotle's Six Top-Level Categories for Existents**:
1. Things
2. Actions
3. Relations
4. Quantities
5. Times
6. Places
