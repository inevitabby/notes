# Argument

**Argument**: An attempt to give evidence that an idea is true.
- "The form we consider evidence"
- Consists of:
	1. **Conclusion**: What you're trying to prove
	2. **Premises**: Supporting evidence
		- Minimum # to make an argument is 1
			* (an argument with one premise is a syllogism)
	3. **Inference**: Connects premises
		- Represented by the word "therefore"

> Example: *"All humans eventually die. I am human. Therefore, I will eventually die."*
> - Conclusion: *I will eventually die*
> - Inference: *Therefore*
> - Premises: *All humans eventually die $\land$ I am human*
> 
> Syllogism Example: *"I know this student is here because I just saw them"*
> - Conclusion: *Student is here*
> - Inference: *Because*
> - Premises: *I just saw them*

**Recognizing Arguments**: A passage is an argument if it *tries* to support a statement logically.
- One *possible* sign of an argument is the use of indicator words like "because" or "since".

**Four Kinds of Passages**:
1. **Narrative**: Series of events
2. **Description**: Give characteristics of something
	- *e.g., "Albert Einstein had white hair"*
3. **Causal Explanation**: Gives cause of something
	- *"My car stopped because it ran out of gas" only describes cause, it doesn't try to argue for it.*
4. **Argument**: Gives evidence to prove an idea is true. 

> *Note: Prof. states that the line between causal explanation and argument is pretty blurry, but still sticks with these older categories and expresses dislike with the new categories in the book...*

**Examples of Indicator Words:**

| Premise       | Conclusion      |
| :---:         | :---:           |
| As            | So              |
| For           | Consequently    |
| Given that    | As a result     |
| Assuming that | It follows that |
| Inasmuch as   | Hence           |

# Diagramming Arguments

**Dependent Premises (+):** When premises depend on each other to get to a conclusion.
- Example:

> <center>I want to graduate in four years + I need 120 units to graduate
> 
> ---
> 
> $\downarrow$
> 
> I need to take an average 30 units per year</center>

**Independent Premises:** When premises don't depend on each other to get to a conclusion.
- Example:

> <center>$x$'s fingerprints are on the weapon. $x$ is the heir of the murdered person
> 
> $\downarrow$
> 
> $x$ is the murderer

**2-Step Argument:**
- Example:

> <center>My car just broke down on the freeway.
> 
> $\downarrow$
> 
> I need a tow (+ I am a AAA member)
> 
> $\downarrow$
> 
> I need to call AAA</center>

## Exercise 2.2

Prompt: (1) I shouldn't go home this weekend not only because (2) I have too much studying to do, but also because (3) I can't afford the trip.

Diagram:

> <center>1 2
> 
> $\downarrow$ $\downarrow$
> 
> 3</center>

# Good Arguments v.s. Bad Arguments

**Good Argument**: Argument that succeeds in proving its conclusion.
- **Two Components of a Good Argument**:
	1. **True Premises**
	2. **Strong Inference**
		- Pretending the premises are true, do they still lead to the conclusion?

**Bad Argument**: Argument that fails to provide evidence for its conclusion.

# Implicit Premises

The implicit premise...
1. ... should narrow the logical gap between the stated premise and the conclusion.
2. ... shouldn't commit to more than is necessary.
	- e.g., "Sally can't go hiking because she has a broken leg" doesn't mean that "someone with a broken leg can't move".

