# To What Extent is Objectivity Possible?

- If objectivity is impossible, then we know nothing about the world
- Objectivity is a work-in-progress.
- You can't just put your emotions aside, but you need to be cautious with them

# Where Mistakes Come In

1. **Misinterpreting Senses**:
	- "Our senses don't lie": Your sense of sight may perceive a mirage, but the lie happens when you *misinterpret* it as water instead of a mirage
2. **Words & Language**:
	- Error can come in as soon as we begin organizing experiences into words.
3. **Pleasure & Pain**:
	- Physical: Errors come in when you relate the feeling to life in general
	- Emotions: Thinking shapes over time what gives you emotional pleasure/pain

**Other People...**:
- ...Are potential sources of every type of mistake above
- ...And they can also willfully lie.

# 3 Basic Requirements of Reliable Testimony

> **Testimony**: Any learning from other people that goes beyond your own direct experience.

1. **Qualifications**
	- "Is this person in a position to have this knowledge?"
		* e.g., Education, training, reputation (references from other qualified)
		* "Was this eyewitness really there/mistaken/etc.?"
2. **Motive / Character**
	- "Does this person care/have reason to lie/focused/etc.?"
3. **Logic** of what the person says
	- e.g., Test results, the person can explain the results to you.
