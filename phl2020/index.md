# PHL2020: Critical Thinking

The following are my class notes for Critical Thinking.

# Topics

1. [How the Mind Works](01 - How the Mind Works.html)
2. [Quiz 1 Review](02 - Quiz 1 Review.html)
3. [Objectivity and Reliable Testimony](03 - Objectivity and Reliable Testimony.html)
4. [Argument](04 - Argument.html)
5. [Fallacies](05 - Fallacies.html)
6. [Parents](06 - Parents.html)
7. [Emotions of Logic](07 - Emotions of Logic.html)
8. [Concepts and Definitions](08 - Concepts and Definitions.html)
