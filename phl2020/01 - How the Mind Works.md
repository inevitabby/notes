# Basic Points on How the Mind Works

- **Philosophy**: Trying to be more scientific and objective about everyday life.
- **Epistemology**: Branch of philosophy that studies the question of "How do we know?"

**"How do we know?"**
1. Knowledge starts from sense-perception
	- e.g., sight, sound, touch, taste, smell, etc.
	- Objections: What about religion?, what about hallucinations?, what about gravity?, what about knowing whether something/someone is trustworthy?
		* Question: How do you know whether you were hallucinating?
			+ Answer: Other people, self-reflection
				+ Which means you need to use your sense perception to figure out if your prior sense perception was false, which is why this is the root of knowledge.
2. Humans use language to put together a bunch of sense perceptions and communicate it with others.
	- e.g., "I know that bush is poisonous because my Mom told me"
3. Knowledge of what's good or bad for us drawn from our experiences with pleasure and pain.
4. There are two levels of mental processing:
	1. Conscious: Slow and controlled
	2. Subconscious: Fast and automatic

# Principles of How the Human Mind Works

1. **Biological Role of Human Mind**: To solve problems in the process of living
	- The mind is not an end in and of itself.
	- Thinking is built on lower levels of life processes
	- Reasoning is built on thinking to expand on more basic life-activities.
2. **Two Levels of the Mind**:
	1. **Conscious**:
		- What you're focusing on right now
		- You directly control it
		- Can only hold a limited number of things
		- Contents come from sense perception, subconscious, and conscious introspection
		- Slow
		- Sensing, thinking in a focused way, noticing what you're feeling, 
		- Can be directly controlled by higher-level logic
	2. **Subconscious**:
		- In the background
		- You don't directly control it
		- Everything retained in memory
		- Contents come from present/past and how your conscious mind processes them
			* How your mind processes things is born, some developed over time
				+ e.g., Children become talkative at different times, learning ethics age ~6, etc.
		- Fast and automatic
		- Emotions / Gut Feeling / Dreams / etc.
		- Programmed by the conscious mind, but also by its own automatic connections
			* "You are your habits"
		- Automatic defense mechanisms
3. **Sense perception is the starting-point for knowledge and constant checkpoint for thinking.**
	- **Five Senses (Extraspection)**: Sight, touch, hearing, taste, and smell
	- **Introspection**: Additional "sixth" sense
		* Direct awareness of ones own mental processes
		* Also includes delving into your subconscious to understand your emotions
		* Noticing and thinking about what your mind is doing.
4. **Language Organizes and Communicates Sense-Perceptions** 
	- Language greatly enhances our sense-perceptions
5. **Emotional/Physical Pleasure & Pain**
	- Tells us good/bad and how to act
6. **Information & Ideas From Other People**
	- Source of most of our knowledge

# Aristotle on Psyche/Soul

1. **Vegetative Soul** (what plants do)
	- Nutrition
	- Growth
	- Reproduction
2. **Animal Soul** (what animals do)
	- Sense Perception
	- Pleasure/Pain
	- Desire/Aversion
	- Locomotion
3. **Human Soul** (what humans do)
	- Reasoning/Thinking in Words, Sentences
	- Complex Emotions.

> **Note**: Every soul has the characteristics of the levels before it.
> - e.g., Animal Soul can do everything Vegetative Soul can, et cetera.

# Professor's Thesis on Pleasure and Pain

Pleasure exists to make you continue doing something (e.g., drinking cold water to satisfy thirst, seeing something beautiful)
- "Good" fundamentally means well-functioning life activity.

Pain exists to tell you whether your life processes are going terribly (e.g., seeing a bright light).
- We regard hurting people as moral issues.

When we put everything together (e.g., pleasure/pain, info from other people, etc.); there is a lot of room for personal values and interpretation, but there is also a lot of commonality in our moral foundations.

# Logic & Objectivity

**Logic**: A way of thinking that is (1) **Rooted in evidence** and (2) **Not self-contradictory**.
- "The art of reasoning" ("art" in the sense that it's an applied science)
- Goal: To find truth

**Truth**: Mental content that conforms to reality.
- Arises only at the same time as the issue of falsehood.

**Law of Non-Contradiction**: Nothing can be both $A$ and $\lnot A$ at the *same time* and in the *same respect*.
- Alternative Phrasing: Law of Identity: $A \equiv A$

**Objectivity**: Choosing to be committed to facts, logic, and truth
- Social aspects:
	* Applying these standards to your own and other's thinking
	* Communicating by presenting your thoughts in context for your audience
- Prioritizing anything above facts, logic, and truth breaks objectivity

