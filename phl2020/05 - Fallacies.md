# Fallacy

**Fallacy**: A counterfeit argument. 
- Appears to be valid at a first glance.

# A. Fallacies of Relevance

1. **Subjectivism**: I believe/want $p$ to be true, therefore $p$.
	- Examples: 
		* "God must exist because I couldn't live if he didn't"
		* "*Barbie* (2023) is a great movie because it made me laugh and kept my interest"
		* "My brother would never do that."
		* "This person is guilty because I think they look suspicious",
	- aka: Fallacy of Appeal to One's Own Emotions/Beliefs
2. **Appeal to Majority**: A majority thinks $p$ is true, therefore $p$.
	- Examples:
		* "Buying slaves is good because nearly every successful country bought slaves."
		* "All the other countries are providing universal healthcare, and therefore the US should too."
		* "A majority of Americans believe we should kill homeless people for sport."
		* "This product gets 5 stars, so it must be good"
3. **Appeal to Audience Emotion**: Lots of emotional language but no evidence, therefore $p$.
	- Examples:
		* "We must kick out the rapists and murders crossing the border"
		* "They are stealing hardworking American's jobs" 
	- aka: Appeal to the mob (Ad Populum), Appeal to Pity
4. **Appeal to Force**: I'll do something bad to you if you don't accept $p$, therefore $p$ is true.
	 - Examples:
		* "If you write something that I disagree with, I will grade you harshly"
		* "This is a good rule because you'll be punished if you don't follow it"
5. **Ad Hominem**: A bad person is advocating $p$, therefore $p$ is false, or their argument is bad.
	- Examples:
		* "Don't vote for this proposition because trial lawyers support it"
		* "You're just saying that because you own a daycare"
	- Special Case: Poisoning the Well (argument is bad because proposer has a vested interest)
	- Special Case: You, Too Fallacy (you also do this)
	- Special Case: Abusive ad hominem

# B. Inductive Fallacies

6. **Appeal to Authority**: A respected person says $p$, therefore $p$.
	- Note: Only a fallacy if the person fails tests for reliable testimony.
	- Examples:
		* "It's true because it's in the Bible."
7. **False Dichotomy**: Either $p$ or $q$. Not $q$, therefore $p$.
8. **Post hoc**: $p$ came before $q$, therefore $p$ caused $q$.
	- Note: This is a special case of correlation $\ne$ causation
9. **Hasty Generalization**: One or a few things show $p$, therefore $p$
10. **Accident**: We have a generalization and falsely apply it to prove $p$
	- Note: This is the reverse of hasty generalization (hasty instantiation?)
	- Example:
		* "A player has 20 seconds to serve starting from the end of the last point in a Tennis game. The player fell down and got injured in the last point and 20 seconds have passed, therefore that player has lost a point."
11. **Slippery Slope**: Action $P \to Q \to R \to S$, therefore we shouldn't do $P$.

# C. Fallacies of Presumption and Diversion

12. **Circular Argument**: $p$ is true because it's true
13. **Equivocation**: ... $W$ ... is true because ....$W$ ... (where $W$ has two different meanings)
14. **Appeal to Ignorance**: $p$ is true because you haven't disproved $p$
	- Why: The burden of proof is on the person making the claims.
15. **Diversion**: $S$ is true because I can prove $T$
16. **Straw Man**: $S$ is false because I can disprove $T$
	- Note: A special version of diversion.
