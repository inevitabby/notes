# Introduction

> **False Dichotomy**: You can either be logical or emotional.
> - Option 3: We can use our rational thinking to examine our emotions.

**Values**: Anything that you act to get/keep.
- Our deepest values come from our childhood.

# Dr. Packer's Theory of Emotion

> **Packer's Thesis**: Behind every emotion there's a (usually implicit) thought that $x$ is good or bad for you.

## 4 Parts of Emotion

1. **Inner feeling**
2. **Usually implicit thought that something's good/bad**
3. **Bodily changes**
	- This is how we can tell another person's emotions before they are even aware of their own emotions.
4. **Action tendency.**

## 6 Steps of Introspection (NUPARC)

1. **Name the Emotion**
2. **Universal Evaluation**
3. **Personal Evaluation**
4. **Assess the truth of U (Universal Evaluation) and P (Personal Evaluation)**
5. **Reasons in the person's psychological history for why this is false**
6. **Correct the thinking (include specific, actionable steps)**

> **NUPARC Example**:
> 1. **N**ame: Anxiety
> 2. **U**niversal Evaluation: I'm incapable of being in a romantic relationship.
> 3. **P**ersonal Evaluation: I'll never give a woman what she wants. I'll never find happiness in romance.
> 4. **A**ssess the truth of U and P: Partly true and partly false. He'll never find happiness if he keeps behaving like this. If he fixes his behavior, he'll be capable.
> 5. **R**easons in the person's psychological history for why his false: He was dumped for no reason he could understand, so he wanted to make sure he's in control in all future relationships (fear of pain).
> 6. **C**orrect the thinking: Find the courage to date women he can have a serious relationship with. I'll eventually find the one.

**Some Common Universal Evaluations**:

| Emotion        | Universal Evaluation                                 |
| :---:          | :---:                                                |
| Fear           | I'm in danger because $x$ is endangering me          |
| Anger          | I'm the victim of an injustice                       |
| Joy            | I'm in pleasure because I achieved something I value |
| Anxiety        | I'm incapable or unworthy                            |
- Note: "I'm" can also refer to "Mine"
	- Example: We can be angry on behalf of our loved ones.

---

- Additional Notes:
	* Anger often is a mix of hurt and disappointment
	* Anxiety feels like a free-floating fear.
- Anxiety Defenses:
	* Repression
	* Denial
	* Projection
	* Withdrawal
	* Hostility
	* etc.

# Aristotle Thoughts on Emotion & Action

**Motivation** is the mental process that leads to action.

**Desire** is the inner urge to act, determines what's worth acting on.

